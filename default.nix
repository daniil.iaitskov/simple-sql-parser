{ system ? builtins.currentSystem or "x86_64-linux", ghc ? "ghc8107" }:

let
  nix = import ./nix;

  pkgs = nix.pkgSetForSystem system {
    config = {
      allowBroken = true;
      allowUnfree = true;
    };
  };

  inherit (pkgs) lib;

  inherit (pkgs.haskell.lib)
    doCheck dontCheck enableCabalFlag enableSharedExecutables;

  haskellPkgSetOverlay = pkgs.callPackage ./nix/haskell/overlay.nix {
    inherit (nix) sources;
  };

  simpleSqlParserSourceRegexes = [
    "^website.*$"
    "^Language.*$"
    "^tools.*$"
    "^.*\\.cabal$"
    "Build.hs"
    "^LICENSE$"
  ];

  simpleSqlParserBase = enableSharedExecutables (doCheck
    (haskellPkgs.callCabal2nix "simple-sql-parser"
      (lib.sourceByRegex ./. simpleSqlParserSourceRegexes) { }));

  simpleSqlParserOverlay = _hfinal: _hprev: { simple-sql-parser = simpleSqlParserBase; };

  baseHaskellPkgs = pkgs.haskell.packages.${ghc};

  haskellOverlays = [ haskellPkgSetOverlay simpleSqlParserOverlay ];

  haskellPkgs = baseHaskellPkgs.override (old: {
    overrides =
      builtins.foldl' pkgs.lib.composeExtensions (old.overrides or (_: _: { }))
      haskellOverlays;
  });

  haskellLanguageServer =
    pkgs.haskell.lib.overrideCabal haskellPkgs.haskell-language-server
    (_: { enableSharedExecutables = true; });

  shell = haskellPkgs.shellFor {
    packages = _: [];

    # include any recommended tools
    nativeBuildInputs = [ haskellLanguageServer ] ++ (with pkgs; [
      cabal-install
      ghcid
      hlint
      niv
      # hpack
    ]);

    shellHook = ''
      echo shell hook
      # bare cabal
      # hpack
    '';
  };

  simple-sql-parser = haskellPkgs.simple-sql-parser;
in {
  inherit haskellPkgs;
  inherit ghc;
  inherit pkgs;
  inherit shell;
  inherit simple-sql-parser;
  inherit haskellOverlays;
  inherit haskellLanguageServer;
}
